﻿# Automata User Guide

## Table of Contents
1. Starting a Project
2. Tests
3. Scripts
4. Actions
5. Macros
6. Performing Local Tests
7. Performing Load Tests

## 1. Starting a Project

In Automata, projects are used to organize Tests, Scripts, and Data Sets with the application they are testing. Each Project should represent a single system or Pega application. To create a new project, navigate to the "Projects" tab and select `Create New Project`. Then, fill out the following four fields:
-   **Project Name**: The name associated with a project
-   **Project Org**: The user group that will have access to the project
-   **Primary URL**: The URL of the main server that testing will be conducted on
-   **Date Created**: A date associated with project creation
-   **Description**: A brief description of the project
	
Once in a project, the project's tests, scripts, and data sets can be viewed, created, edited, and deleted. 

## 2. Tests

A Test represents the procedure for progressing a business process from creation to completion. Tests can be viewed, created, edited, and deleted from inside their associated project. To create a test, enter the corresponding project, navigate to the "Tests" tab, and click "Add New Test." Then fill out the following four fields:
-   **Title**: The name associated with a project
-   **URL**: The URL that will be used to run a test. This will override the URL of any scripts.
-   **Description**: A brief description of the project
	
Tests are made up of one or many scripts. Add preexisting scripts to tests by checking out a test, and selecting the  `+`  button. Scripts are created inside of the project’s “Scripts” tab.

When creating or editing a test, it is important to understand the differences between check in/check out and saving. Note that these features work the same whether working with tests, scripts, macros or datasets.
-   **Check In/Out**: Creates a copy of the test for the user to edit, while the original is still available to be used by other users. Checking the test in will copy over the original test with the new changes and allow other users can make edits when needed.
-   **Save As**: Saves the current test as a new test.
-   **Save**: Saves the current version of the test.

## 3. Scripts

A Script represents a single user's role in progressing a business process. Most scripts will follow a pattern of: logging in as a user, performing work as that users, and finally logging off as that user. Scripts are the building blocks of tests, and consist of a series of actions. To create a script, navigate to a project,  select the scripts tab, and select “Add New Script.” Then, fill out the following four fields:
-   **Title**: The name associated with a script
-   **URL**: When running an individual script by performing a local test, this URL will be used. However, when a script is run as part of a test, the test URL will override this script URL. 
-   **Pega Version**: The version of Pega this script will run on
-   **Description**: A brief description of the script

## 4. Actions

An action represents a single interaction with the screen, such as clicking a button or typing in a text box. Both Scripts and Macros are composed of Actions. Actions can be added to a Script or Macro by clicking the  `+`  button in the respective editor. Select the action type to fill out specific information about the action.

### Action Types

-   **Cache Value**: Creates and/or sets a variable with the text value of an element in the UI.
-   **Clear Field**: Clears the value of an element.
-   **Click**: Clicks an element.
-   **Default Frame**: Switches to search for elements in the top-level HTML document.
-   **Dropdown**: Selects a value for a dropdown element from the available options. The option is selected by its visible text.
-   **End Script**: Performs several clean-up steps for script processing. This should be the final action in each script.
-   **Enter Random Row**: Selects a random row from a table. The selector for this action should point at a table element. All following actions will only use elements contained in the selected row. The selectors for these action should be within the scope of the table row element.
-   **Exit Row**: Allows for the selection of elements outside of a row selected by the Enter Random Row action. All following actions will use the HTML document containing the current table row.
-   **File Upload**: Uploads a file to the target system.
-   **Hover**: Hovers over an element to display any menus or menu options. Hover actions can be chained in a sequence to allow interactions with elements nested inside of several menus.
-   **Parent Frame**: Switches to search for elements in the HTML document containing the current HTML document. Used to exit an IFrame from the Switch Frame action.
-	**Store Constant**: Creates and/or sets a variable with a value from the user.
-	**Switch Frame**: Switches to search for elements in an HTML document nested inside the current HTML document. The selector for this action should point to an IFrame containing the embedded document.
-	**Text Input**: Enters a user-specified value into a text field.

### Selectors

Selectors define which HTML element Automata will interact with for a given action. Automata provides some predefined wrappers for common selectors as well as the option for more advanced selection using pure CSS or XPATH.

- **Aria-Label**: HTML attribute similar to Title. Shows text on hover of an HTML element.
- **CSS**: Any valid CSS selector. Consult CSS standards for more information.
- **ID**: HTML attribute for id.
- **Name**: HTML name attribute
- **Pega Test ID**: Pega generated HTML attribute that is configurable from Pega's section editor. Common feature on most Pega inputs.
- **Text**: The visibile text of the element. For example, a button that says "Submit".
- **Title**: HTML attribute that shows informative text on hover of the element.
- **XPath**: Any valid XPath selector. Consult XPath documentation for more information.

#### Best Practices for Selectors

Choosing the most approptiates selectos for an given action is at the core of developing Tests with Automata. More often than not, bugs in Scripts can be resolved by editing the selector used for that action. Some best practices:

- There will always be more than one way to select any given element. It's best to configure a selector that is the most specific.
- Modern browsers usually have an option to auto-generate an XPath or CSS selector. This is **NOT ADVISED** as the selector usually isn't precise enough to consistently select the correct element.
- The ID attribute on Pega elements isn't necessarily reliable. Pega eschews best practice for using the ID attribute and assignes the same value to multiple elements. Sometimes it will even change the value each time a page is brought up.
- Pega Test ID tends to be very reliable. However it isn't always present as a specific access group is required for it to render in the UI. Additionally it can be changed from the section editor. Use carefully.
- Text, Title, and Name are often the simplest and most obvious selectors. When using them, make sure that no other element on the screen matches the exact value of the target element. If there are multiple elements with the same value on screen, Automata may not always select the correct one. If this causes issues, it is recommended to either use a more precise value for your selector or to refine the selector using XPath. 

### Variables

Automata supports variables to make Scripts and Tests more dynamic and reliable. Setting the value of a variable can be done by using the Store Constant or Cache Value actions. The value stored in the variable can be used as the text of a Text Input or Dropdown action, or for the value of a selector. The syntax for using a variable is by wrapping the variable name like so:  `{% var %}`. Automata will replace this string with the value of the variable  _var_.

Variables and their values will persist between Scripts in the same Test. The values of variables in previous Scripts will override the values defined in later Scripts. It is always advised to set the value of any variables you plan to use in a Script, even if you expect the value to be set in an earlier Script of the Test. This practice makes debugging individual Scripts less problematic and acts as a default value if for whatever reason the variable isn't properly set earlier.

#### Unique

The  `{% unique %}`  variable will insert a new globally unique identifier each time it is used.

## 5. Macros

A macro is a small set of actions that is available across different scripts. Macros allow for the reuse of interactions. Many scripts may share interactions such as logging in or out of the application. By using a macro to perform these interactions, the same group of actions can occur in many scripts without the need to add the actions individually each time. Because of the nature of macros it is highly recommended to make use of variables. Any variables will be prompted for values when adding the Macro to a Script.

Macros can be managed from the  `Macros`  tab in the navigation menu. Macros can be added to scripts in the same way as an action. Simply select the desired Macro instead of an Action Type.

## 6. Performing Local Tests

Local tests allow users to visually see the test as it is being performed. Local tests are useful for smoke and regression testing, as well as building new scripts. Running tests and scripts locally require several components:

-   Java 8
-   The Automata jar file
-   automata.properties
-   chromedriver or geckodriver

### Set-up

#### automata.properties

The automata.properties file contains the values for several key system variables needed to run tests. This file must be located in the same directory as the Automata jar file. An example automata.properties file is shown below.

```
## Username and password for connecting to the Automata server
username=AutomataUser
password=Pa$$W0rd

## Domain of the Automata server
host=srcLogic.com/automata

## Logging level can be None, Info, Debug, or All
loglevel=All

## Browser to use for execution
browser=firefox

##Path to the web driver
chromedriver=/Path/to/driver.exe
geckodriver=/Path/to/driver.exe

```

### chromedriver or geckodriver

These web drivers allow Automata to open a browser session that it can interact with. Only the driver that corresponds to the selected browser type is needed to run tests. Whichever driver you choose, it should match the version of the browser on your local machine. The automata.properties files will need the file paths to the chosen driver as well. These paths are relative for windows and absolute for Linux. You can download the  [chromedriver](https://sites.google.com/a/chromium.org/chromedriver/)  or the  [geckodriver](https://github.com/mozilla/geckodriver/releases)  from their respective websites.

### Running a test or script

The Automata.jar is a java executable file that performs the tests or scripts made on the Automata platform. As it is a java file, the local machine will need to have Java installed. The executable can be ran from the command line with a command similar to the following

```
> java -jar Automata.jar test "Project Name" "Test Name"

```

The command takes four arguments:

-   the type of executable to run (test or script)
-   the name of the project containing the executable
-   the name of the executable
-   **Optional for Tests Only:**  the number of executables to perform concurrently (up to 5)

## 7. Performing Load Tests

Load test will run concurrent versions of the same test in order to imitate many users on the application at the same time. These test can be run from the Automata interface. To run a load test, navigate to the desired test. Click the `Run Load Test` button to open the Load Test modal. Here you can enter a test name for records, as well as select the type of test and how many users you want to simulate. The Simple Scaling test will begin with a single user and ramp up to the specified run count following the Fibonacci sequence; the Burst test will run the test with the specified run count; and the Soak test will run the test with the specified run count for the specified duration in minutes. Click the  `Submit`  button to start the test. The screen will display the results screen with real-time test performance metrics.
