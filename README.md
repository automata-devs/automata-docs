# Automata Docs

Documentation for Automata

## Usage

From the command line, run the main.py file and answer the prompts to add a release note to the system. The system will automatically update latest.json and all.json with the new information.

## Schema

### latest
latest.json contains the most recent version of each component's release notes as one JSON object keyed by the service
```
{
    service: // service name,
    version: // version of the service,
    timestamp: // UTC timestamp at creation,
    notes: {
        short: // short tagline for the release,
        long: [
            // list of long form changes in the release
        ]
    }
}
```

### all
all.json contains all the release notes for automata as a JSON array where each element has the same schema as in latest.json
