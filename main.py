from datetime import datetime
import json

class _notes:
    short: str
    long: list

    def __init__(self):
        self.long = list()
        self.short = None

class ReleaseNote:
    service: str
    version: str
    timestamp: datetime
    notes: _notes

    def __init__(self, service: str = None, version: str = None):
        if service is not None:
            self.service = service
        if version is not None:
            self.version = version
        self.timestamp = datetime.utcnow()
        self.notes = _notes()

    def add_short_note(self, s_note: str) -> None:
        self.notes.short = s_note

    def add_long_note(self, l_note:str) -> None:
        self.notes.long.append(l_note)

    def toDict(self) -> str:
        return {
            'service': self.service,
            'version': self.version,
            'timestamp': str(self.timestamp),
            'notes': self.notes.__dict__
        }
    
    @classmethod
    def fromDict(cls, d: dict):
        r = ReleaseNote()
        for k in ReleaseNote.__dict__.keys():
            if k in d:
                r.__setattr__(k, d[k])
        return r

if __name__ == "__main__":
    print("Preparing to create a new release note for Automata")
    ser = input('What is the service you are releasing? ')
    ver = input('What version of {ser} is this? '.format(ser=ser))
    r = ReleaseNote(ser, ver)
    r.add_short_note(input('What is the short note for this release? '))
    l00p = True
    while l00p:
        add_long = input('Do you want to add more notes? [Y/n] ')
        if add_long.upper() == 'Y' or add_long == "":
            r.add_long_note(input('What is your note? '))
        else:
            l00p = False
    
    latest = open('./release-notes/latest.json', 'r+')
    if latest.read() is not "":
        latest.seek(0)
        d = json.loads(latest.read())
    else:
        d = dict()
    d.update({ser: r.toDict()})
    latest.seek(0)
    latest.write(json.dumps(d))

    all = open('./release-notes/all.json', 'r+')
    if all.read() is not "":
        all.seek(0)
        a = json.loads(all.read())
    else:
        a = list()
    all.seek(0)
    all.write(json.dumps([r.toDict()] + a))